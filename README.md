# Yoctix

### Yoctix - is a GNU/Linux distribution, one can build using Yocto build system.

The current build is available for a generic x86_64 platform with UEFI boot loader. It uses XFCE4 as a desktop environment.

To build the distro, please read https://docs.yoctoproject.org/brief-yoctoprojectqs/index.html , install host dependencies and try building the `bitbake core-image-sato` target. Afterwards initialize the Yoctix project with `./yoctix_init.sh` script, `source oe-init-build-env` and run `bitbake core-image-minimal-xfce`. Build process may fail due to lack of resources, and you can rerun it a couple of more times until you succeed.
If your machine is powerful enough, you may tune or comment out `BB_NUMBER_THREADS` and `PARALLEL_MAKE` variables in `distro_template.conf`, then run `./yoctix_init.sh` again.
After a successful build, please run `runqemu qemux86-64 qemuparams="-m 2048 -enable-kvm -display sdl"` to see the result.
