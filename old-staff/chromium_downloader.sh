#!/bin/bash
VERSION=$(curl -Ss https://ungoogled-software.github.io/ungoogled-chromium-binaries/releases/linux_portable/64bit/ \
	| grep ungoogled-chromium-binaries | grep '<li>' | head -n1 | cut -d'"' -f2 | cut -d'/' -f6)

echo "Downloading Ungoogled Chromium ${VERSION} Please wait..."
cd /usr/local
wget -c https://github.com/Eloston/ungoogled-chromium-binaries/releases/download/${VERSION}/ungoogled-chromium_${VERSION}_linux.tar.xz
echo "Unpacking, please wait..."
tar xf ungoogled-chromium_${VERSION}_linux.tar.xz
ls -Flash

cd /usr/local/bin
ln -sf /usr/local/ungoogled-chromium_${VERSION}_linux/chrome

echo "
[Desktop Entry]
Categories=Network;WebBrowser;
Encoding=UTF-8
Name=Ungoogled Chromium
GenericName=Ungoogled Chromium
Comment=Ungoogled Chromium
Exec=/usr/local/bin/chrome
Icon=/usr/local/ungoogled-chromium_${VERSION}_linux/product_logo_48.png
Terminal=false
StartupNotify=true
MimeType=text/html;text/xml;application/xhtml+xml;text/mml;x-scheme-handler/http;x-scheme-handler/https;
Type=Application
" > /usr/share/applications/ungoogled-chromium.desktop 

echo "
[Desktop Entry]
Categories=Network;WebBrowser;
Encoding=UTF-8
Name=Ungoogled Chromium Without Sandbox
GenericName=Ungoogled Chromium Without Sandbox
Comment=Ungoogled Chromium Without Sandbox
Exec=sh -c \"/usr/local/bin/chrome --no-sandbox\"
Icon=/usr/local/ungoogled-chromium_${VERSION}_linux/product_logo_48.png
Terminal=false
StartupNotify=true
MimeType=text/html;text/xml;application/xhtml+xml;text/mml;x-scheme-handler/http;x-scheme-handler/https;
Type=Application
" > /usr/share/applications/ungoogled-chromium-no-sandbox.desktop
