#!/bin/bash

DIRLIST=$(ls -1 | grep 'meta\|poky\|yocto')
for i in $DIRLIST
do
    cd $i
    echo "=== Updating $i"
    git pull
    git status
    cd ..
done
