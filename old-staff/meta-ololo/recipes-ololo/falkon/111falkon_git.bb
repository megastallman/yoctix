DESCRIPTION = "falkon browser"
LICENSE = "GPL-3.0"
LIC_FILES_CHKSUM = "file://COPYING;md5=8f0e2cd40e05189ec81232da84bd6e1a"
#LIC_FILES_CHKSUM=file://LICENSE.GPLv3;md5=8f0e2cd40e05189ec81232da84bd6e1a"
inherit qmake5 cmake systemd
require recipes-qt/qt5/qt5-git.inc

SRC_URI = "git://anongit.kde.org/falkon.git"
SRCREV = "${AUTOREV}"
DEPENDS = "qtbase qtdeclarative qtwebengine extra-cmake-modules cmake"
S = "${WORKDIR}/git"
PACKAGECONFIG ?= ""
PACKAGECONFIG[desktop] = "-DDESKTOP_BUILD,,"

RDEPENDS_${PN} += " \
    extra-cmake-modules \
    qtvirtualkeyboard \
    qtquickcontrols \
    qtwebengine \
    qtgraphicaleffects \
    qtmultimedia \
"
