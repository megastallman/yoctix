SUMMARY = "Falkon"
HOMEPAGE = "https://www.falkon.org/"
LICENSE = "GPL3"
LIC_FILES_CHKSUM = "file://COPYING;md5=8f0e2cd40e05189ec81232da84bd6e1a"

DEPENDS = "extra-cmake-modules qtwebengine"

export QT_INSTALL_PREFIX
export OE_QMAKE_PATH_EXTERNAL_HOST_BINS

SRC_URI = "https://download.kde.org/stable/falkon/3.0.1/falkon-${PV}.tar.xz"
SRC_URI[md5sum] = "c6fb6433ac0f228667e2f763c02317c5"
#SRC_URI[sha256sum] = ""

EXTRA_OECMAKE = "-DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_BUILD_TYPE=Release -DBUILD_TESTING=OFF"

inherit pkgconfig cmake

do_install() {
    install -d ${D}${bindir}
    install -m 0755 falkon ${D}${bindir}    
}
