SUMMARY = "NCurses Disk Usage"
HOMEPAGE = "https://dev.yorhel.nl/ncdu"
SECTION = "console/utils"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://COPYING;md5=6682a63e3e3e8ea14051d05056a857a0"

DEPENDS = "ncurses"

SRC_URI = "https://dev.yorhel.nl/download/ncdu-${PV}.tar.gz"
SRC_URI[md5sum] = "67239592ac41f42290f52ab89ff198be"
SRC_URI[sha256sum] = "f4d9285c38292c2de05e444d0ba271cbfe1a705eee37c2b23ea7c448ab37255a"

inherit autotools pkgconfig
