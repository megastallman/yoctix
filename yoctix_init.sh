#!/usr/bin/bash


WORKING_DIR=$(pwd)

add_oe_layers () {
  cd ${WORKING_DIR}/yoctix/poky
  . oe-init-build-env
  bitbake-layers add-layer ../../meta-openembedded/meta-oe
  bitbake-layers add-layer ../../meta-openembedded/meta-python
  bitbake-layers add-layer ../../meta-openembedded/meta-perl
  bitbake-layers add-layer ../../meta-openembedded/meta-multimedia
  bitbake-layers add-layer ../../meta-openembedded/meta-networking
  bitbake-layers add-layer ../../meta-openembedded/meta-gnome
  bitbake-layers add-layer ../../meta-openembedded/meta-xfce
}

recreate_local_conf () {
    echo "Recreating local.conf"
    rm -rf ${WORKING_DIR}/yoctix/poky/build/conf/local.conf
    cd ${WORKING_DIR}/yoctix/poky
    . oe-init-build-env
    cat ${WORKING_DIR}/distro_template.conf >> ${WORKING_DIR}/yoctix/poky/build/conf/local.conf
    cat ${WORKING_DIR}/yoctix/poky/build/conf/local.conf
}

if [ ! -d "${WORKING_DIR}/yoctix" ]; then
  echo "Initializing Yoctix build directory..."
  mkdir yoctix && cd yoctix
  git clone git://git.yoctoproject.org/poky
  git clone https://github.com/openembedded/meta-openembedded
  add_oe_layers
else
  cd ${WORKING_DIR}/yoctix/poky && git pull
  cd ${WORKING_DIR}/yoctix/meta-openembedded && git pull
fi

recreate_local_conf
